The library provides the SimpleXbox360Controller class as a simple mean to access Xinput. 
This class allow your program to communicate with any Xbox 360 controller connected to your system.
It only works under windows, since on linux xpad and xboxdrv provides standard accesses to the controller
All the code is under GPL license.

There is basically 2 ways to achieve this :
* Calling the update() method, and then getCurrentState()
* Using the autopolling feature, which periodically check the controller by calling update() and waiting for signals sent if the state of the controller changed.

Just remember things are only updated when calling update(). This can be done automatically using autopolling, or manually by calling update().
Important

Add this line in your .pro file otherwise the compiler will NOT find the Xinput library :
win32:LIBS += $${_PRO_FILE_PWD_}/SimpleXbox360Controller/XInput.lib
Constructor

Take up to 5 parameters
1. The associated controller number (from 0 to 3). If omitted, 0 is used.
2. The left stick deadzone radius (from 0 to 32767). If omitted, a nice default value is used.
3. The right stick deadzone radius(from 0 to 32767). If omitted, a nice default value is used.
4. The triggers threshold value (from 0 to 255). If omitted, a nice default value is used.
5. The parent QObject.(can be omitted too)

One instance of SimpleXbox360Controller handles one and only one controller. If you need 4 controllers handling, use 4 instances.
The InputState class

this class contains the state of the controller.
It belongs to the SimpleXbox360Controller class (For example, use the following code to instantiate it : SimpleXbox360Controller::InputState monInputState.
Each stick is represented by 2 float values from -1 to 1 (0 is the idle state).
Each trigger is represented by a float value from 0 to 1.
These value are obtained after processing the deadzone, so you don't have to deal with deadzones calculations.
The battery type and battery values are both 8 bits unsigned integers, that must be compared with Xinput.h BATTERY_TYPE_ and BATTERY_LEVEL_ definitions.
The buttons state are represented by a 16 bit unsigned integer. The state of each button can be checked using InputState::isButtonPressed() with Xinput.h definitions : to check if A is pressed :
myInputState.isButtonPressed(XINPUT_GAMEPAD_A) return True if A is pressed
Signals

    controllerNewState(SimpleXbox360Controller::InputState) : this signal is emitted when the state of the controller changed since the previous call of update().
    controllerNewBatteryState(quint8 newBatteryType, quint8 newBatteryLevel) : this signal is emitted when the state of battery of the controller changed since the previous call of update(). The parameters must be interpreted by comparison with Xinput.h BATTERY_TYPE_ and BATTERY_LEVEL_ definitions.
    void controllerConnected(unsigned int controllerNum) : emitted when the controller has been connected since the previous call of update().
    void controllerDisconnected(unsigned int controllerNum) : emitted when the controller has been disconnected since the previous call of update().

Slots

    update() allow you to manually retrieve the current state of the controller. It can then be accessed using getCurrentState()
    startAutoPolling(unsigned int interval) start a QTimer which will calls update() periodically, "interval' being the delay between two calls. Your program will be noticed of any controller change by the signals.
    stopAutoPolling() : stop the QTimer.
    void setVibration(float leftVibration, float rightVibration) : set the vibrations level for left motor and right motor (they produces different feelings). Values are between 0 and 1

Deadzones settings :

    setLeftStickDeadZone(unsigned int newDeadZone) : set the left stick deadzone radius (between 0 and 32767).
    setRightStickDeadZone(unsigned int newDeadZone) : set the right stick deadzone radius (between 0 and 32767).
    setTriggerThreshold(unsigned int newThreshold) : set the triggers threshold (between 0 and 255).

Other Methods :

    bool isStateChanged(void) : return true if controller state changed since the previous call of update()
    bool isConnected(void) : return true if the controlled is connected



Resources :

	xbox 360 controller picture from http://www.spyparty.com/2010/08/30/have-an-alpha-masked-xbox-360-controller-on-me/comment-page-1/?replytocom=5573
	box 360 controller icon from SvenGraph http://www.svengraph.net