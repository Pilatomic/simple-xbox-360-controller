#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    controller1 = new SimpleXbox360Controller(0);
    controller2 = new SimpleXbox360Controller(1);
    controller3 = new SimpleXbox360Controller(2);
    controller4 = new SimpleXbox360Controller(3);

    controller1->startAutoPolling(20);
    controller2->startAutoPolling(20);
    controller3->startAutoPolling(20);
    controller4->startAutoPolling(20);

    connect(controller1,SIGNAL(controllerNewState(SimpleXbox360Controller::InputState)),ui->tab1,SLOT(displayGamepadState(SimpleXbox360Controller::InputState)));
    connect(controller2,SIGNAL(controllerNewState(SimpleXbox360Controller::InputState)),ui->tab2,SLOT(displayGamepadState(SimpleXbox360Controller::InputState)));
    connect(controller3,SIGNAL(controllerNewState(SimpleXbox360Controller::InputState)),ui->tab3,SLOT(displayGamepadState(SimpleXbox360Controller::InputState)));
    connect(controller4,SIGNAL(controllerNewState(SimpleXbox360Controller::InputState)),ui->tab4,SLOT(displayGamepadState(SimpleXbox360Controller::InputState)));

    connect(controller1,SIGNAL(controllerConnected(uint)),ui->tab1,SLOT(GamepadConnected()));
    connect(controller2,SIGNAL(controllerConnected(uint)),ui->tab2,SLOT(GamepadConnected()));
    connect(controller3,SIGNAL(controllerConnected(uint)),ui->tab3,SLOT(GamepadConnected()));
    connect(controller4,SIGNAL(controllerConnected(uint)),ui->tab4,SLOT(GamepadConnected()));

    connect(controller1,SIGNAL(controllerDisconnected(uint)),ui->tab1,SLOT(GamepadDisconnected()));
    connect(controller2,SIGNAL(controllerDisconnected(uint)),ui->tab2,SLOT(GamepadDisconnected()));
    connect(controller3,SIGNAL(controllerDisconnected(uint)),ui->tab3,SLOT(GamepadDisconnected()));
    connect(controller4,SIGNAL(controllerDisconnected(uint)),ui->tab4,SLOT(GamepadDisconnected()));

    connect(controller1,SIGNAL(controllerConnected(uint)),this,SLOT(controlledConnected(uint)));
    connect(controller2,SIGNAL(controllerConnected(uint)),this,SLOT(controlledConnected(uint)));
    connect(controller3,SIGNAL(controllerConnected(uint)),this,SLOT(controlledConnected(uint)));
    connect(controller4,SIGNAL(controllerConnected(uint)),this,SLOT(controlledConnected(uint)));
}

void MainWindow::controlledConnected(uint controllerNum){
    ui->tabWidget->setCurrentIndex(controllerNum);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete controller1;
    delete controller2;
    delete controller3;
    delete controller4;
}
