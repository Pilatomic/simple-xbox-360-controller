#-------------------------------------------------
#
# Project created by QtCreator 2014-02-23T16:59:36
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = simpleXbox360controller
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    SimpleXbox360Controller/simplexbox360controller.cpp \
    gamepaddisplay.cpp

HEADERS  += mainwindow.h \
    SimpleXbox360Controller/XInput.h \
    SimpleXbox360Controller/simplexbox360controller.h \
    gamepaddisplay.h

FORMS    += mainwindow.ui

win32:LIBS += $${_PRO_FILE_PWD_}/SimpleXbox360Controller/XInput.lib

OTHER_FILES += \
    SimpleXbox360Controller/XInput.lib

RESOURCES += \
    resources.qrc
