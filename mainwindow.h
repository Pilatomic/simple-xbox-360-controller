#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include "SimpleXbox360Controller/simplexbox360controller.h"
#include "gamepaddisplay.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void controlledConnected(uint controllerNum);
private:
    Ui::MainWindow *ui;
    SimpleXbox360Controller* controller1;
    SimpleXbox360Controller* controller2;
    SimpleXbox360Controller* controller3;
    SimpleXbox360Controller* controller4;
};

#endif // MAINWINDOW_H
